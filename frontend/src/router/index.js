import Vue from 'vue';
import VueRouter from 'vue-router';
import Login from '../views/Login.vue';
import Index from '../views/Index.vue';
import Users from '../views/user/Users.vue';
import Teams from '../views/team/Teams.vue';
import Admins from '../views/admin/Admins.vue';
import Control from '../views/control/Control.vue';

Vue.use(VueRouter);

const routes = [
  { path: '/', redirect: '/login' },
  { path: '/login', component: Login },
  {
    path: '/index',
    component: Index,
    children: [
      { path: '/control', component: Control },
      { path: '/user', component: Users },
      { path: '/team', component: Teams },
      { path: '/admin', component: Admins },
    ],
  },
];

const router = new VueRouter({
  mode: 'hash',
  routes,
});

// 挂在路由导航守卫
router.beforeEach((to, from, next) => {
  // to代表将要访问的路径
  // from代表从哪个路径而来
  // next表示放行函数
  if (to.path === '/login') return next();
  const token = window.sessionStorage.getItem('token');
  if (!token) return next('/login');
  next();
});

export default router;
