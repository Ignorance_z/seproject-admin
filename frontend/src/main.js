import Vue from 'vue';
import App from './App.vue';
import router from './router';
// 导入axios包
import axios from 'axios';
// 配置请求的根路径
axios.defaults.baseURL = 'http://127.0.0.1:8000/apps/';
// 加载 element 组件库
import ElementUI from 'element-ui';
// 加载 element 组件库的样式
import 'element-ui/lib/theme-chalk/index.css';
import "./assets/global.css"

axios.interceptors.request.use((config) => {
  config.headers.Authorization = window.sessionStorage.getItem('token');
  return config;
});

// 加上如下定义可以使得每一个vue组件都可以通过this直接访问到http从而发起ajax请求
Vue.prototype.$http = axios;

Vue.use(ElementUI);
Vue.config.productionTip = false;

new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app');
