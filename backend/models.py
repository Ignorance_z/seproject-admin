# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Admins(models.Model):
    admin_id = models.BigAutoField(primary_key=True)
    admin_name = models.CharField(max_length=10, blank=True, null=True)
    admin_role = models.CharField(max_length=255, blank=True, null=True)
    admin_gender = models.CharField(max_length=1, blank=True, null=True)
    password = models.CharField(max_length=100, blank=True, null=True)
    email = models.CharField(max_length=255, blank=True, null=True)
    admin_introduction = models.CharField(max_length=200, blank=True, null=True)
    trade = models.CharField(max_length=100, blank=True, null=True)
    register_time = models.CharField(max_length=20, blank=True, null=True)
    user_avatar = models.CharField(max_length=755, blank=True, null=True)
    is_email_verify = models.IntegerField(blank=True, null=True)
    verify_code = models.CharField(max_length=12, blank=True, null=True)
    verify_code_time = models.CharField(max_length=50, blank=True, null=True)
    build_team = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'admins'


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.IntegerField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class Controlpanel(models.Model):
    panel_id = models.BigAutoField(primary_key=True)
    panel_title = models.CharField(max_length=32)
    parent_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'controlpanel'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.PositiveSmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    id = models.BigAutoField(primary_key=True)
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class FileTable(models.Model):
    feature_id = models.CharField(primary_key=True, max_length=32)
    user_id = models.IntegerField(blank=True, null=True)
    user_name = models.CharField(max_length=20, blank=True, null=True)
    file_size = models.BigIntegerField(blank=True, null=True)
    upload_time = models.CharField(max_length=20, blank=True, null=True)
    group_id = models.BigIntegerField(blank=True, null=True)
    link = models.CharField(max_length=755, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'file_table'


class Message(models.Model):
    message_id = models.BigAutoField(primary_key=True)
    title = models.CharField(max_length=255, blank=True, null=True)
    type = models.CharField(max_length=1, blank=True, null=True)
    content = models.CharField(max_length=255, blank=True, null=True)
    user_id = models.BigIntegerField(blank=True, null=True)
    user_name = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'message'


class MsgUser(models.Model):
    msg_user_id = models.BigAutoField(primary_key=True)
    message_id = models.BigIntegerField(blank=True, null=True)
    user_id = models.BigIntegerField(blank=True, null=True)
    time = models.CharField(max_length=60, blank=True, null=True)
    is_look = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'msg_user'


class Note(models.Model):
    note_id = models.BigIntegerField(primary_key=True)
    make_user_id = models.BigIntegerField(blank=True, null=True)
    access_user_id = models.BigIntegerField(blank=True, null=True)
    time = models.CharField(max_length=60, blank=True, null=True)
    content = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'note'


class Notice(models.Model):
    notice_id = models.BigAutoField(primary_key=True)
    notice_title = models.CharField(max_length=32, blank=True, null=True)
    create_time = models.CharField(max_length=20, blank=True, null=True)
    update_time = models.CharField(max_length=20, blank=True, null=True)
    content = models.TextField(blank=True, null=True)
    group_id = models.CharField(max_length=255, blank=True, null=True)
    user_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'notice'


class OptRecode(models.Model):
    recode_id = models.BigAutoField(primary_key=True)
    group_id = models.BigIntegerField(blank=True, null=True)
    group_name = models.CharField(max_length=30, blank=True, null=True)
    user_id = models.BigIntegerField(blank=True, null=True)
    user_name = models.CharField(max_length=30, blank=True, null=True)
    opt_id = models.IntegerField(blank=True, null=True)
    opt_name = models.CharField(max_length=10, blank=True, null=True)
    opt_date = models.CharField(max_length=20, blank=True, null=True)
    user_role = models.IntegerField(blank=True, null=True)
    user_role_name = models.CharField(max_length=5, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'opt_recode'


class OptResult(models.Model):
    id = models.BigAutoField(primary_key=True)
    recode_id = models.BigIntegerField(blank=True, null=True)
    user_name = models.CharField(max_length=20, blank=True, null=True)
    score = models.FloatField(blank=True, null=True)
    result = models.CharField(max_length=4, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'opt_result'


class Roles(models.Model):
    role_id = models.BigAutoField(primary_key=True)
    role_name = models.CharField(max_length=10)
    user_id = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'roles'


class Team(models.Model):
    group_id = models.BigAutoField(primary_key=True)
    group_name = models.CharField(max_length=16)
    file_capacity = models.FloatField(blank=True, null=True)
    capacity = models.IntegerField(blank=True, null=True)
    slogan = models.CharField(max_length=32, blank=True, null=True)
    group_info = models.CharField(max_length=255, blank=True, null=True)
    group_avatar = models.CharField(max_length=755, blank=True, null=True)
    register_time = models.CharField(max_length=20, blank=True, null=True)
    access_code = models.CharField(max_length=10, blank=True, null=True)
    current_file_size = models.FloatField(blank=True, null=True)
    current_member_size = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'team'


class UserGroup(models.Model):
    id = models.BigAutoField(primary_key=True)
    user_role = models.CharField(max_length=1)
    user_id = models.BigIntegerField()
    group_id = models.CharField(max_length=255)
    is_access = models.IntegerField()
    is_input_voice = models.CharField(max_length=1, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'user_group'


class Users(models.Model):
    user_id = models.BigAutoField(primary_key=True)
    user_name = models.CharField(max_length=10, blank=True, null=True)
    user_gender = models.CharField(max_length=1, blank=True, null=True)
    password = models.CharField(max_length=100, blank=True, null=True)
    email = models.CharField(max_length=255, blank=True, null=True)
    user_introduction = models.CharField(max_length=200, blank=True, null=True)
    trade = models.CharField(max_length=100, blank=True, null=True)
    register_time = models.CharField(max_length=20, blank=True, null=True)
    user_avatar = models.CharField(max_length=755, blank=True, null=True)
    is_email_verify = models.IntegerField(blank=True, null=True)
    verify_code = models.CharField(max_length=12, blank=True, null=True)
    verify_code_time = models.CharField(max_length=50, blank=True, null=True)
    build_team = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'users'


class VipTable(models.Model):
    vip_id = models.IntegerField(primary_key=True)
    file_capacity = models.CharField(max_length=255, blank=True, null=True)
    capacity = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'vip_table'


class Voiceinfo(models.Model):
    voice_id = models.BigAutoField(primary_key=True)
    feature_id = models.CharField(max_length=255)
    encoding = models.CharField(max_length=255)
    rate = models.IntegerField()
    channels = models.IntegerField(blank=True, null=True)
    bit_depth = models.IntegerField(blank=True, null=True)
    status = models.IntegerField()
    audio = models.CharField(max_length=500)
    lib_id = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'voiceinfo'


class Voicelibrary(models.Model):
    lib_id = models.CharField(primary_key=True, max_length=255)
    lib_title = models.CharField(max_length=32, blank=True, null=True)
    lib_introduction = models.CharField(max_length=255, blank=True, null=True)
    create_time = models.CharField(max_length=20, blank=True, null=True)
    update_time = models.CharField(max_length=20, blank=True, null=True)
    group_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'voicelibrary'
