from datetime import datetime
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import serializers
from rest_framework.viewsets import ModelViewSet
from rest_framework.filters import SearchFilter

from ..utils.returnCode import SUCCESS, SEARCH_FAIL, REGISTER_FAIL, DELETE_FAIL
from ..utils.usPagination import UsPageNumberPagination
from ..utils.verifyCodeGenerator import generate_code
from .models import Users
from datetime import date, timedelta


# Create your views here.
# 目前已经加入  全部用户列表查询、单个用户列表查询（使用user_id）、新用户注册、用户删除  四项基本功能


# 用户序列化器
class UserListCheckSerializers(serializers.ModelSerializer):
    register_time = serializers.DateTimeField(format="%Y-%m-%d")

    class Meta:
        model = Users
        fields = "__all__"


# 用户注册序列化器
class UserRegisterSerializers(serializers.ModelSerializer):
    register_time = serializers.CharField(default='')
    build_team = serializers.IntegerField(default=0)
    # 表示管理员添加
    is_email_verify = serializers.IntegerField(default=1)
    password = serializers.CharField(write_only=True)
    newStr = generate_code(6)
    verify_code = serializers.CharField(default=newStr)
    user_id = serializers.IntegerField(read_only=True)

    class Meta:
        model = Users
        fields = ["user_id", "user_name", "user_gender", "password", "email",
                  "register_time", "build_team", "verify_code", "is_email_verify"]


"""

    用户列表分页查询，可以使用模糊查询

"""


class UserListCheckViewSet(ModelViewSet):
    queryset = Users.objects.all().order_by("user_id")
    serializer_class = UserListCheckSerializers
    pagination_class = UsPageNumberPagination
    filter_backends = (SearchFilter,)
    search_fields = ('user_name',)


"""
    实现单个用户用户名查询(暂未使用)
"""


class UserSingleCheckView(APIView):
    def get(self, request, user_id):
        try:
            user = UserListCheckSerializers(Users.objects.get(user_id=user_id))
            return Response({"code": SUCCESS, "msg": "查询成功！", "data": user.data})
        except Users.DoesNotExist as e:
            return Response({"code": SEARCH_FAIL, "msg": "该用户不存在！"})


"""
    实现用户信息的注册
"""


class UserRegisterView(APIView):
    def post(self, request):
        request.data['register_time'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        ser = UserRegisterSerializers(data=request.data)
        if ser.is_valid():
            ser.save()
            return Response({"code": SUCCESS, "msg": "注册成功！", "data": ser.data})
        else:
            return Response({"code": REGISTER_FAIL, "msg": "注册失败！"})


"""
    实现用户信息的删除
"""


class UserDeleteView(APIView):
    def delete(self, request, user_id):
        try:
            user = Users.objects.get(user_id=user_id)
            user.delete()
            return Response({"code": SUCCESS, "msg": "删除成功！"})
        except Users.DoesNotExist as e:
            return Response({"code": DELETE_FAIL, "msg": "用户不存在！"})


"""

    获取用户总数量

"""


class GetTotalUserNum(APIView):
    def get(self, request):
        count = Users.objects.all().count()
        return Response({'code': SUCCESS, 'data': count})


"""
    
    获取当天注册用户数
    
"""


class GetTodayRegisterUserNum(APIView):
    def get(self, request):
        # 获取今天的起始时间
        today_start = date.today()
        # 查询当天注册的用户数量
        today_user_count = Users.objects.filter(register_time__gte=today_start).count()
        return Response({"code": SUCCESS, "data": today_user_count})


"""

    获取过去七天注册用户变化

"""


class UserRegistrationTrendView(APIView):

    def get(self, request):
        # 获取当前日期
        today_date = date.today()
        start_date = today_date - timedelta(6)
        date_list = []

        for i in range(7):
            # 循环遍历获取当天日期
            index_date = start_date + timedelta(days=i)
            # 指定下一天日期
            cur_date = start_date + timedelta(days=i + 1)
            # 查询条件是大于当前日期index_date，小于明天日期的用户cur_date，得到当天用户量
            count = Users.objects.filter(register_time__gte=index_date, register_time__lt=cur_date).count()

            date_list.append({
                'count': count,
                'date': index_date
            })

        return Response({"code": SUCCESS, "data": date_list})
