from django.db import models


# Create your models here.
class Users(models.Model):
    user_id = models.BigAutoField(primary_key=True)
    user_name = models.CharField(max_length=10, blank=True, null=True)
    user_gender = models.CharField(max_length=1, blank=True, null=True)
    password = models.CharField(max_length=100, blank=True, null=True)
    email = models.CharField(max_length=255, blank=True, null=True)
    user_introduction = models.CharField(max_length=200, blank=True, null=True)
    trade = models.CharField(max_length=100, blank=True, null=True)
    register_time = models.CharField(max_length=20, blank=True, null=True)
    user_avatar = models.CharField(max_length=755, blank=True, null=True)
    is_email_verify = models.IntegerField(blank=True, null=True)
    # 验证码
    verify_code = models.CharField(max_length=12, blank=True, null=True)
    verify_code_time = models.CharField(max_length=50, blank=True, null=True)
    build_team = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'users'