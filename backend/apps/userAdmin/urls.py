# -*- coding: utf-8 -*-
from django.urls import path
from rest_framework.routers import DefaultRouter

from . import views

router = DefaultRouter()
router.register(r'userLists', viewset=views.UserListCheckViewSet)

urlpatterns = [
    path('userSingle/<user_id>', views.UserSingleCheckView.as_view()),
    path('userRegister/', views.UserRegisterView.as_view()),
    path('userDelete/<user_id>', views.UserDeleteView.as_view()),
    path('getTodayUserNum/', views.GetTodayRegisterUserNum.as_view()),
    path('getTotalUserNum/', views.GetTotalUserNum.as_view()),
    path('getUserRegistrationTrend/', views.UserRegistrationTrendView.as_view()),
]
urlpatterns += router.urls
