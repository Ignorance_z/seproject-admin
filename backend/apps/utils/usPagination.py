# -*- coding: utf-8 -*-
import array

from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from ..utils.returnCode import SUCCESS


class UsPageNumberPagination(PageNumberPagination):
    page_size = 10
    page_query_description = "page size query description"
    page_size_query_param = 'size'
    page_query_param = 'page'
    max_page_size = 15

    def get_paginated_response(self, data):
        return Response({
            'code': SUCCESS,
            'links': {
                'next': self.get_next_link(),
                'previous': self.get_previous_link(),
            },
            'count': self.page.paginator.count,
            'results': data
        })