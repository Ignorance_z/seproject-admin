# -*- coding: utf-8 -*-
import datetime
from django.conf import settings
import jwt


# 参数1需要传入payload参数作为加密的第二部分，参数2为默认超时时间，为1分钟
def create_token(payload, timeout=1):
    headers = {
        'typ': 'jwt',
        'alg': 'HS256'
    }
    payload['exp'] = datetime.datetime.utcnow() + datetime.timedelta(timeout)
    # 形成token
    result = jwt.encode(headers=headers, payload=payload, key=settings.SECRET_KEY, algorithm="HS256")
    return result
