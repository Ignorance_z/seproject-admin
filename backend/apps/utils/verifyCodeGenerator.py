# -*- coding: utf-8 -*-
import random


def generate_code(length):
    characters = 'abcdefghijklmnopgrstuwxyzABCDEFGHIJKLMNOPORSTUWXYZ0123456789'
    random_string = ''.join(random.choice(characters) for _ in range(length))
    return random_string
