from django.db import models


# Create your models here.

class Team(models.Model):
    group_id = models.BigAutoField(primary_key=True)
    group_name = models.CharField(max_length=16)
    file_capacity = models.FloatField(blank=True, null=True)
    capacity = models.IntegerField(blank=True, null=True)
    slogan = models.CharField(max_length=32, blank=True, null=True)
    group_info = models.CharField(max_length=255, blank=True, null=True)
    group_avatar = models.CharField(max_length=755, blank=True, null=True)
    register_time = models.CharField(max_length=20, blank=True, null=True)
    # 团队邀请码
    access_code = models.CharField(max_length=10, blank=True, null=True)
    current_file_size = models.FloatField(blank=True, null=True)
    current_member_size = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'team'


class UserGroup(models.Model):
    id = models.BigAutoField(primary_key=True)
    user_role = models.CharField(max_length=1)
    user_id = models.BigIntegerField()
    group_id = models.CharField(max_length=255)
    is_access = models.IntegerField()
    is_input_voice = models.CharField(max_length=1, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'user_group'