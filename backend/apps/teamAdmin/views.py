from django.db.models import Count
from django.db.models.functions import Cast
from django.forms import DateField
from django.shortcuts import render
from datetime import datetime, date, timedelta
from rest_framework.filters import SearchFilter
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import serializers
from rest_framework.viewsets import ModelViewSet
from .models import Team, UserGroup
from ..userAdmin.models import Users
from ..userAdmin.views import UserSingleCheckView
from ..utils.returnCode import SUCCESS, SEARCH_FAIL, REGISTER_FAIL, DELETE_FAIL, UPDATE_FAIL
from ..utils.usPagination import UsPageNumberPagination
from ..utils.verifyCodeGenerator import generate_code


# 用户序列化器
class UsersSerializers(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = ['user_name']


# 团队信息序列化器
class GroupSerializers(serializers.ModelSerializer):
    group_id = serializers.IntegerField(read_only=True)
    file_capacity = serializers.FloatField(default=40)
    capacity = serializers.IntegerField(default=10)
    slogan = serializers.CharField(default="此群主很懒，什么都没有留下~")
    current_file_size = serializers.FloatField(default=0)
    current_member_size = serializers.IntegerField(default=0)
    register_time = serializers.CharField(default='')
    access_code = serializers.CharField(default=generate_code(8))

    class Meta:
        model = Team
        fields = '__all__'


# 用户团队表序列化器
class UserGroupSerializers(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    user_role = serializers.IntegerField(default=0)
    is_access = serializers.IntegerField(default=1)
    is_input_voice = serializers.IntegerField(default=0)

    class Meta:
        model = UserGroup
        fields = '__all__'


"""

    团队列表分页查询(允许使用模糊查询)

"""


class TeamListCheckViewSet(ModelViewSet):
    queryset = Team.objects.all().order_by("group_id")
    serializer_class = GroupSerializers
    pagination_class = UsPageNumberPagination
    filter_backends = (SearchFilter,)
    search_fields = ('group_name',)


"""

    进行单个团队查询（group_id）

"""


class TeamSingleCheckView(APIView):
    def get(self, request, group_id):
        try:
            team = GroupSerializers(Team.objects.get(group_id=group_id))
            return Response({"code": SUCCESS, "data": team.data, "msg": "查询成功！"})
        except Team.DoesNotExist as e:
            return Response({"code": SEARCH_FAIL, "msg": "该团队不存在！"})


"""

    团队注册（检验是否该用户存在，进行团队创建，插入用户——团队表）

"""


class TeamRegisterView(APIView):
    def post(self, request):
        # 去除里面包含的user信息
        team_data, user_data = request.data['team'], request.data['user']
        # 将请求结果进行分解处理，进行传入数值的合法性校验
        team_data['register_time'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        ser_team, ser_user = GroupSerializers(data=team_data), UsersSerializers(data=user_data)
        # 判断传入数据是否正确
        if not ser_user.is_valid():
            return Response({"code": REGISTER_FAIL, "msg": "数据校验错误！"})
        try:
            user = Users.objects.filter(user_name=user_data.get('user_name')).first()
            user_id = user.user_id
        except Users.DoesNotExit as e:
            return Response({"code": REGISTER_FAIL, "msg": "该指定用户不存在！"})

        # 进行团队数据保存
        if not ser_team.is_valid():
            return Response({"code": REGISTER_FAIL, "msg": "数据校验错误！"})

        ser_team.save()

        # 增加用户的build_team字段
        user = Users.objects.get(user_id=user_id)
        user.build_team += 1
        user.save()

        # 保存进入成员团队表，注册成功
        user_group = {"user_id": user_id, "group_id": ser_team.data.get('group_id')}
        ser_user_group = UserGroupSerializers(data=user_group)
        if ser_user_group.is_valid():
            ser_user_group.save()
            return Response({"code": SUCCESS, "msg": "团队注册成功！"})
        else:
            return Response({"code": REGISTER_FAIL, "msg": "数据校验错误！"})


"""

    团队删除

"""


class TeamDeleteView(APIView):
    def delete(self, request, group_id):
        try:
            group = Team.objects.get(group_id=group_id)
        except Team.DoesNotExist as e:
            return Response({"code": DELETE_FAIL, "msg": "团队不存在！"})

        try:
            info = UserGroup.objects.filter(group_id=group_id)
        except UserGroup.DoesNotExit as e:
            return Response({"code": DELETE_FAIL, "msg": "团队不存在！"})

        group.delete()
        info.delete()
        return Response({"code": SUCCESS, "msg": "团队删除成功！"})


"""

    进行团队内容修改

"""


class TeamUpdateView(APIView):
    def patch(self, request, group_id):
        try:
            team = Team.objects.get(group_id=group_id)
            ser = GroupSerializers(team, data=request.data, partial=True)
        except Team.DoesNotExit as e:
            return Response({"code": UPDATE_FAIL, "msg": "该团队不存在！"})

        if ser.is_valid():
            ser.save()
            return Response({"code": SUCCESS, "msg": "修改成功！"})
        return Response({"code": UPDATE_FAIL, "msg": "团队信息修改异常！"})


"""

    获取用户总数量

"""


class GetTotalTeamNum(APIView):
    def get(self, request):
        count = Team.objects.all().count()
        return Response({'code': SUCCESS, 'data': count})


"""

    获取当天注册团队总数

"""


class GetTodayRegisterTeamNum(APIView):
    def get(self, request):
        # 获取今天的起始时间
        today_start = date.today()
        # 查询当天注册的用户数量
        today_team_count = Team.objects.filter(register_time__gte=today_start).count()
        return Response({"code": SUCCESS, "data": today_team_count})


"""

    获取过去七天注册团队变化

"""


class TeamRegistrationTrendView(APIView):

    def get(self, request):
        # 获取当前日期
        today_date = date.today()
        start_date = today_date - timedelta(6)
        date_list = []

        for i in range(7):
            # 循环遍历获取当天日期
            index_date = start_date + timedelta(days=i)
            # 指定下一天日期
            cur_date = start_date + timedelta(days=i + 1)
            # 查询条件是大于当前日期index_date，小于明天日期的用户cur_date，得到当天用户量
            count = Team.objects.filter(register_time__gte=index_date, register_time__lt=cur_date).count()

            date_list.append({
                'count': count,
                'date': index_date
            })

        return Response({"code": SUCCESS, "data": date_list})
