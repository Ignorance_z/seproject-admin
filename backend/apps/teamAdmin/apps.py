from django.apps import AppConfig


class TeamadminConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.teamAdmin'
