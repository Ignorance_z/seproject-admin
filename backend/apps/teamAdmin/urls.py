# -*- coding: utf-8 -*-
from django.urls import path, include
from . import views
from rest_framework.routers import SimpleRouter, DefaultRouter

router = DefaultRouter()
router.register(r'teamLists', viewset=views.TeamListCheckViewSet)

urlpatterns = [
    path('teamSingle/<group_id>', views.TeamSingleCheckView.as_view()),
    path('teamRegister/', views.TeamRegisterView.as_view()),
    path('teamDelete/<group_id>', views.TeamDeleteView.as_view()),
    path('teamUpdate/<group_id>', views.TeamUpdateView.as_view()),
    path('getTodayTeamNum/', views.GetTodayRegisterTeamNum.as_view()),
    path('getTotalTeamNum/', views.GetTotalTeamNum.as_view()),
    path('getTeamRegistrationTrend/', views.TeamRegistrationTrendView.as_view()),
]
urlpatterns += router.urls
