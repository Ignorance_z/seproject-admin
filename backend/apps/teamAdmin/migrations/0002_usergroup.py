# Generated by Django 4.1 on 2023-12-01 12:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('teamAdmin', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserGroup',
            fields=[
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
                ('user_role', models.CharField(max_length=1)),
                ('user_id', models.BigIntegerField()),
                ('group_id', models.CharField(max_length=255)),
                ('is_access', models.IntegerField()),
                ('is_input_voice', models.CharField(blank=True, max_length=1, null=True)),
            ],
            options={
                'db_table': 'user_group',
                'managed': True,
            },
        ),
    ]
