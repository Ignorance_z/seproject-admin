import hashlib

from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.views import APIView
from drf_yasg2.utils import swagger_auto_schema
from drf_yasg2 import openapi
from . import models
from ..utils.jwtAuth import create_token
from ..utils.returnCode import SUCCESS, LOGIN_FAIL, CHECK_FAIL


class LoginSerializers(serializers.ModelSerializer):
    class Meta:
        model = models.Admins
        fields = ['admin_name', 'password']


class LoginView(APIView):
    authentication_classes = []

    def post(self, request):
        username = request.data.get('username')
        request.data['password'] = hashlib.md5(request.data['password'].encode(encoding="UTF-8")).hexdigest()
        password = request.data.get('password')
        print(request.data['password'])
        ser = LoginSerializers(data=request.data)

        if not ser.is_valid():
            return Response({"code": CHECK_FAIL, "msg": '校验失败'})

        admin = models.Admins.objects.filter(admin_name=username, password=password).first()
        if not admin:
            return Response({"code": LOGIN_FAIL, "msg": '用户名或密码错误'})

        payload = {
            'username': dict(ser.data).get('username'),
            'admin_id': admin.admin_id
        }
        # 确定token有效时间
        token = create_token(payload, 60)
        return Response({"code": SUCCESS, "msg": "token生成成功！", "token": token})
