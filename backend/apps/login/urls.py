# -*- coding: utf-8 -*-
from django.urls import path
from . import views

urlpatterns = [
    # path('test/', views.LoginView.as_view())
    path('adminLogin/', views.LoginView.as_view())
]