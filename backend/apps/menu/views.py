from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from ..utils.returnCode import SUCCESS


class MenuView(APIView):
    def get(self, request):
        data = [
            {"id": 101, "authName": "主页", "path": "/main", "children": [
                {"id": '101-1', "authName": "中控台", "path": "/control", "children": []}
            ]},
            {"id": 102, "authName": "用户", "path": None, "children": [
                {"id": '102-1', "authName": "用户列表", "path": "/user", "children": []}
            ]},
            {"id": 103, "authName": "团队", "path": None, "children": [
                {"id": '101-1', "authName": "团队列表", "path": "/team", "children": []}
            ]},
            {"id": 104, "authName": "管理员", "path": None, "children": [
                {"id": '101-1', "authName": "管理员列表", "path": "/admin", "children": []}
            ]}
        ]
        return Response({"code": SUCCESS, "msg": "菜单获取成功！", "data": data})
