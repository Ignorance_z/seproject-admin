# -*- coding: utf-8 -*-
from django.urls import path
from . import views

urlpatterns = [
    path('getMenu/', views.MenuView.as_view()),
]
