import hashlib
from datetime import datetime, date, timedelta
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import serializers
from django.utils import timezone
from rest_framework.viewsets import ModelViewSet
from rest_framework.filters import SearchFilter
from rest_framework_jwt.utils import jwt_decode_handler

from ..utils.returnCode import SUCCESS, SEARCH_FAIL, REGISTER_FAIL, DELETE_FAIL
from ..utils.usPagination import UsPageNumberPagination
from ..utils.verifyCodeGenerator import generate_code
from ..login.models import Admins


# 管理员序列化器
class AdminListCheckSerializers(serializers.ModelSerializer):
    register_time = serializers.DateTimeField(format="%Y-%m-%d")

    class Meta:
        model = Admins
        fields = "__all__"


# 管理员注册序列化器
class AdminRegisterSerializers(serializers.ModelSerializer):
    register_time = serializers.CharField(default='')
    build_team = serializers.IntegerField(default=0)
    # 表示管理员添加
    is_email_verify = serializers.IntegerField(default=1)
    password = serializers.CharField(write_only=True)
    newStr = generate_code(6)
    verify_code = serializers.CharField(default=newStr)
    admin_id = serializers.IntegerField(read_only=True)
    admin_role = serializers.CharField(default='0', write_only=True)

    class Meta:
        model = Admins
        fields = ["admin_id", "admin_name", "admin_role", "admin_gender", "password", "email",
                  "register_time", "build_team", "verify_code", "is_email_verify"]


"""

    管理员列表分页查询，可以使用模糊查询

"""


class AdminListCheckViewSet(ModelViewSet):
    queryset = Admins.objects.all().order_by("admin_id")
    serializer_class = AdminListCheckSerializers
    pagination_class = UsPageNumberPagination
    filter_backends = (SearchFilter,)
    search_fields = ('admin_name',)


"""
    实现token单个管理员用户名查询
"""


class UserSingleCheckView(APIView):
    def get(self, request, token):

        admin_id = jwt_decode_handler(token).get('admin_id')
        try:
            admin = AdminListCheckSerializers(Admins.objects.get(admin_id=admin_id))
            return Response({"code": SUCCESS, "msg": "查询成功！", "data": admin.data})
        except Admins.DoesNotExist as e:
            return Response({"code": SEARCH_FAIL, "msg": "该用户不存在！"})


"""
    实现管理员信息的注册
"""


class AdminRegisterView(APIView):
    def post(self, request):
        request.data['register_time'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        request.data['password'] = hashlib.md5(request.data['password'].encode(encoding="UTF-8")).hexdigest()
        request.data['email'] = hashlib.md5(request.data['email'].encode(encoding="UTF-8")).hexdigest()
        ser = AdminRegisterSerializers(data=request.data)
        if ser.is_valid():
            ser.save()
            return Response({"code": SUCCESS, "msg": "注册成功！", "data": ser.data})
        else:
            return Response({"code": REGISTER_FAIL, "msg": "注册失败！"})


"""
    实现管理员信息的删除
"""


class AdminDeleteView(APIView):
    def delete(self, request, admin_id):
        try:
            user = Admins.objects.get(admin_id=admin_id)
            user.delete()
            return Response({"code": SUCCESS, "msg": "删除成功！"})
        except Admins.DoesNotExist as e:
            return Response({"code": DELETE_FAIL, "msg": "用户不存在！"})


"""

    获取当天注册管理员总数

"""


class GetTodayRegisterAdminNum(APIView):
    def get(self, request):
        # 获取今天的起始时间
        today_start = date.today()
        # 查询当天注册的用户数量
        today_team_count = Admins.objects.filter(register_time__gte=today_start).count()
        return Response({"code": SUCCESS, "data": today_team_count})


"""

    获取管理员总数量

"""


class GetTotalAdminNum(APIView):
    def get(self, request):
        count = Admins.objects.all().count()
        return Response({'code': SUCCESS, 'data': count})


"""

    获取过去七天注册团队变化

"""


class AdminRegistrationTrendView(APIView):

    def get(self, request):
        # 获取当前日期
        today_date = date.today()
        start_date = today_date - timedelta(6)
        date_list = []

        for i in range(7):
            # 循环遍历获取当天日期
            index_date = start_date + timedelta(days=i)
            # 指定下一天日期
            cur_date = start_date + timedelta(days=i + 1)
            # 查询条件是大于当前日期index_date，小于明天日期的用户cur_date，得到当天用户量
            count = Admins.objects.filter(register_time__gte=index_date, register_time__lt=cur_date).count()

            date_list.append({
                'count': count,
                'date': index_date
            })

        return Response({"code": SUCCESS, "data": date_list})
