# -*- coding: utf-8 -*-
from django.urls import path
from rest_framework.routers import DefaultRouter

from . import views

router = DefaultRouter()
router.register(r'adminLists', viewset=views.AdminListCheckViewSet)

urlpatterns = [
    path('adminSingle/<token>', views.UserSingleCheckView.as_view()),
    path('adminRegister/', views.AdminRegisterView.as_view()),
    path('adminDelete/<admin_id>', views.AdminDeleteView.as_view()),
    path('getTodayAdminNum/', views.GetTodayRegisterAdminNum.as_view()),
    path('getTotalAdminNum/', views.GetTotalAdminNum.as_view()),
    path('getAdminRegistrationTrend/', views.AdminRegistrationTrendView.as_view()),
]
urlpatterns += router.urls
